import random


class Robot:
    speed = 0
    runtime = 0
    range = 0
    is_enabled = False
    enabled_message = "Don't bite the robot"
    disabled_message = "Connection can't be established."

    def __init__(self, robot_type):
        self.robot_type = robot_type
        self.charge = random.randint(0, 100)

    def start_operation(self):
        self.is_enabled = True
        print(self.enabled_message + ' ' + str(self.robot_type) + ' welcomes you!')

    def stop_operation(self):
        self.is_enabled = False
        print(self.disabled_message + ' ' + str(self.robot_type) + ' says goodbye to you!')

    def report_change(self):
        print('Current charge level is: ' + str(self.charge))

    def report_speed(self):
        print('Max speed is: ' + str(self.speed))

    def total_runtime(self):
        print('The total runtime is: ' + str(self.runtime))

    def report_range(self):
        print('The total range is: ' + str(self.range))


class SpotMini(Robot):
    step_height = 30
    steps = 0

    def report_steps(self):
        for i in range(self.runtime):
            self.steps = self.steps + random.randint(1, self.runtime)
        print("Steps done:" + str(self.steps))


class HandleRobot(Robot):
    max_mass = 15

    def move_box(self, box_height):
        if box_height > self.max_mass:
            print("Box weights too much!")
        else:
            print('Box moved.')


class Pick(Robot):
    boxes_in_hour = 720

    def do_job(self, hours):
        n_boxes = self.boxes_in_hour * hours
        print("Boxes done:" + str(n_boxes))


class Atlas(Robot):
    is_turbo = False

    def turbo_mode(self):
        self.is_turbo = True
        self.speed = 2
        print("The speed was increased from 1.5 m/s to " + str(self.speed))


basic_robot = Robot('MyRobot')
basic_robot.speed = 2.5
basic_robot.runtime = 125
basic_robot.range = 50
basic_robot.start_operation()
basic_robot.report_range()
basic_robot.report_change()
basic_robot.report_speed()
basic_robot.total_runtime()
basic_robot.stop_operation()

advanced_robot = SpotMini('MiniRobot')
advanced_robot.start_operation()
advanced_robot.runtime = 10
advanced_robot.report_steps()
advanced_robot.stop_operation()
