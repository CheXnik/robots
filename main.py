from math import sqrt


class Shape:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


class Point(Shape):
    def compute_distance(self):
        distance = sqrt(self.x ** 2 + self.y ** 2)
        return distance

    def __init__(self, x=0, y=0):
        super().__init__(x, y)
        self.distance = Point.compute_distance(self)


class Circle(Shape):
    def __init__(self, x, y, radius):
        super().__init__(x, y)
        self.radius = radius

    def is_point(self, point):
        if point.distance <= self.radius:
            return False
        else:
            return True


p = Point(10, 20)
c = Circle(0, 0, 15)
print(c.is_point(p))
